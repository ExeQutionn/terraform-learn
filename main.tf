provider "aws" {
    region = "eu-west-3"
}

variable "cidr_blocks" {
    description = "cidr blocks and names for vpc and subnets"
    type = list(object({
        cidr_block = string
        name = string
    }))    
    /*
    default vrednost se koristi ako se 
    ne pronadje cidr block u terraform.tfvars
    ================================================
    type precizira koje vrednosti su prihvacene 
    ===============================================  
    list(string) - tu cemo da rasporedimo listu 
    stringova za vpc i subnet 
    */
}

variable avail_zone {
  
}

resource "aws_vpc" "development-vpc" {
    /*
    zbog list(string) cemo pored promenjive 
    cidr_blocks staviti u viticastim zagradama 
    broje elementa[]
    */
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        //KEY-VALUE PAIR(MOZE BILO STA)
        Name = var.cidr_blocks[0].name
    } 
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = var.avail_zone
    tags = {
        //KEY-VALUE PAIR(MOZE BILO STA)
        Name = var.cidr_blocks[1].name
    }   
}

